var EXPORTED_SYMBOLS = [ "gtk" ];

const LIBNAME = "gtk-x11-2.0";
const ABIS = [ 0 ];

const Cu = Components.utils;
const Cc = Components.classes;
const Ci = Components.interfaces;

Cu.import("resource://gre/modules/ctypes.jsm");
Cu.import("chrome://topmenu/content/ctypes-utils.js");
Cu.import("chrome://topmenu/content/glib.js");
Cu.import("chrome://topmenu/content/gobject.js");
Cu.import("chrome://topmenu/content/gdk.js");
Cu.import("chrome://topmenu/content/gdk-pixbuf.js");

function defines(lib) {
	this.GtkWidget = gobject.GObject;
	this.GtkBin = this.GtkWidget;
	this.GtkMenuShell = this.GtkWidget;
	this.GtkMenu = this.GtkMenuShell;
	this.GtkItem = this.GtkWidget;
	this.GtkMenuItem = this.GtkItem;
	this.GtkCheckMenuItem = this.GtkMenuItem;
	this.GtkImageMenuItem = this.GtkMenuItem;
	this.GtkImage = this.GtkWidget;

	this.GtkAccelGroup = gobject.GObject;

	this.GtkIconSize = glib.guint;
	this.GTK_ICON_SIZE_INVALID = 0;
    this.GTK_ICON_SIZE_MENU = 1;
    this.GTK_ICON_SIZE_SMALL_TOOLBAR = 2;
    this.GTK_ICON_SIZE_LARGE_TOOLBAR = 3;
    this.GTK_ICON_SIZE_BUTTON = 4;
    this.GTK_ICON_SIZE_DND = 5;
    this.GTK_ICON_SIZE_DIALOG = 6;

	this.GtkAccelFlags = glib.guint;
	this.GTK_ACCEL_VISIBLE = 1 << 0;
	this.GTK_ACCEL_LOCKED = 1 << 1;

	this.GtkItemSelect = ctypes.FunctionType(ctypes.default_abi,
											 ctypes.void_t,
										     [this.GtkItem.ptr, glib.gpointer]);
	this.GtkItemDeselect = this.GtkItemSelect;

	this.GtkMenuItemActivate = ctypes.FunctionType(ctypes.default_abi,
												   ctypes.void_t,
												   [this.GtkMenuItem.ptr, glib.gpointer]);

	lib.lazy_bind("gtk_widget_show", ctypes.void_t, this.GtkWidget.ptr);
	lib.lazy_bind("gtk_widget_show_all", ctypes.void_t, this.GtkWidget.ptr);
	lib.lazy_bind("gtk_widget_destroy", ctypes.void_t, this.GtkWidget.ptr);
	lib.lazy_bind("gtk_widget_add_accelerator", ctypes.void_t, this.GtkWidget.ptr, glib.gchar.ptr, this.GtkAccelGroup.ptr, glib.guint, gdk.GdkModifierType, this.GtkAccelFlags);
	lib.lazy_bind("gtk_widget_mnemonic_activate", ctypes.void_t, this.GtkWidget.ptr, glib.gboolean);
	lib.lazy_bind("gtk_widget_set_sensitive", ctypes.void_t, this.GtkWidget.ptr, glib.gboolean);
	lib.lazy_bind("gtk_widget_set_visible", ctypes.void_t, this.GtkWidget.ptr, glib.gboolean);

	lib.lazy_bind("gtk_bin_get_child", this.GtkWidget.ptr, this.GtkBin.ptr);

	lib.lazy_bind("gtk_menu_shell_insert", ctypes.void_t, this.GtkMenuShell.ptr, this.GtkWidget.ptr, glib.gint);
	lib.lazy_bind("gtk_menu_shell_append", ctypes.void_t, this.GtkMenuShell.ptr, this.GtkWidget.ptr);

	lib.lazy_bind("gtk_menu_new", this.GtkWidget.ptr);

	lib.lazy_bind("gtk_menu_item_new", this.GtkWidget.ptr);
	lib.lazy_bind("gtk_menu_item_new_with_mnemonic", this.GtkWidget.ptr, glib.gchar.ptr);
	lib.lazy_bind("gtk_menu_item_set_label", ctypes.void_t, this.GtkMenuItem.ptr, glib.gchar.ptr);
	lib.lazy_bind("gtk_menu_item_set_submenu", ctypes.void_t, this.GtkMenuItem.ptr, this.GtkWidget.ptr);
	lib.lazy_bind("gtk_check_menu_item_new_with_mnemonic", this.GtkWidget.ptr, glib.gchar.ptr);
	lib.lazy_bind("gtk_check_menu_item_set_active", ctypes.void_t, this.GtkImageMenuItem.ptr, glib.gboolean);
	lib.lazy_bind("gtk_check_menu_item_set_draw_as_radio", ctypes.void_t, this.GtkImageMenuItem.ptr, glib.gboolean);
	lib.lazy_bind("gtk_image_menu_item_new_from_stock", this.GtkWidget.ptr, glib.gchar.ptr, this.GtkAccelGroup.ptr);
	lib.lazy_bind("gtk_image_menu_item_new_with_mnemonic", this.GtkWidget.ptr, glib.gchar.ptr);
	lib.lazy_bind("gtk_image_menu_item_set_image", ctypes.void_t, this.GtkImageMenuItem.ptr, this.GtkWidget.ptr);
	lib.lazy_bind("gtk_separator_menu_item_new", this.GtkWidget.ptr);

	lib.lazy_bind("gtk_image_new_from_pixbuf", this.GtkWidget.ptr, gdk_pixbuf.GdkPixbuf.ptr);
	lib.lazy_bind("gtk_image_new_from_stock", this.GtkWidget.ptr, glib.gchar.ptr, this.GtkIconSize);

	lib.lazy_bind("gtk_accel_group_new", this.GtkAccelGroup.ptr);
}

new ctypes_library(LIBNAME, ABIS, defines, this);
