var EXPORTED_SYMBOLS = [ "glib" ];

const LIBNAME = "glib-2.0";
const ABIS    = [ 0 ];

const Cu = Components.utils;
const Cc = Components.classes;
const Ci = Components.interfaces;

Cu.import("resource://gre/modules/ctypes.jsm");
Cu.import("chrome://topmenu/content/ctypes-utils.js");

function defines(lib) {
	this.gpointer = ctypes.voidptr_t;
	this.gulong = ctypes.unsigned_long;
	this.guint = ctypes.unsigned_int;
	this.guint8 = ctypes.uint8_t;
	this.guint16 = ctypes.uint16_t;
	this.guint32 = ctypes.uint32_t;
	this.gint = ctypes.int;
	this.gint8 = ctypes.int8_t;
	this.gint16 = ctypes.int16_t;
	this.gint32 = ctypes.int32_t;
	this.gchar = ctypes.char;
	this.guchar = ctypes.unsigned_char;
	this.gboolean = this.gint;
	this.gfloat = ctypes.float;
	this.gdouble = ctypes.double;
	this.gsize = ctypes.unsigned_long;
	this.GCallback = ctypes.voidptr_t;
	this.GClosureNotify = this.gpointer;
	this.GFunc = ctypes.void_t.ptr;
};

new ctypes_library(LIBNAME, ABIS, defines, this);
