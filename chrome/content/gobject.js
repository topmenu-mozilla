var EXPORTED_SYMBOLS = [ "gobject" ];

const LIBNAME = "gobject-2.0";
const ABIS = [ 0 ];

const Cu = Components.utils;
const Cc = Components.classes;
const Ci = Components.interfaces;

Cu.import("resource://gre/modules/ctypes.jsm");
Cu.import("chrome://topmenu/content/ctypes-utils.js");
Cu.import("chrome://topmenu/content/glib.js");

function defines(lib) {
	this.GObject = ctypes.StructType("GObject");
	this.GCallback = glib.gpointer;
	this.GClosureNotify = glib.gpointer;
	this.GConnectFlags = glib.guint;
	this.G_CONNECT_AFTER = 1 << 0;
	this.G_CONNECT_SWAPPED = 1 << 1;
	this.GParamSpec = ctypes.StructType("GParamSpec");
	this.GObjectNotifyCallback = ctypes.FunctionType(ctypes.default_abi,
													 ctypes.void_t,
												     [this.GObject.ptr, this.GParamSpec.ptr, glib.gpointer]);

	lib.lazy_bind("g_object_unref", ctypes.void_t, this.GObject.ptr);
	lib.lazy_bind("g_object_ref", this.GObject.ptr, this.GObject.ptr);
	lib.lazy_bind("g_object_ref_sink", this.GObject.ptr, this.GObject.ptr);

	lib.lazy_bind("g_signal_connect_data", glib.gulong, this.GObject.ptr, glib.gchar.ptr, this.GCallback, glib.gpointer, this.GClosureNotify, this.GConnectFlags);
	lib.lazy_bind("g_signal_handler_disconnect", ctypes.void_t, this.GObject.ptr, glib.gulong);

	this.ref = function(obj) {
		this.g_object_ref(obj);
		return ctypes.CDataFinalizer(obj, this.g_object_unref);
	}
	this.ref_sink = function(obj) {
		this.g_object_ref_sink(obj);
		return ctypes.CDataFinalizer(obj, this.g_object_unref);
	}
	this.signal_connect = function(obj, sign, callback, data) {
		return this.g_signal_connect_data(obj, sign, callback, data, null, 0);
	}
}

new ctypes_library(LIBNAME, ABIS, defines, this);
