"use strict";

var EXPORTED_SYMBOLS = [ "vkgdkmap" ];

const Cu = Components.utils;

Cu.import("chrome://topmenu/content/gdk.js");

var vkgdkmap = {
	VK_TAB : gdk.GDK_KEY_Tab,
	VK_RETURN : gdk.GDK_KEY_Return,

	VK_HOME : gdk.GDK_KEY_Home,
	VK_END : gdk.GDK_KEY_End,

	VK_DELETE : gdk.GDK_KEY_Delete,

	VK_F1 : gdk.GDK_KEY_F1,
	VK_F2 : gdk.GDK_KEY_F2,
	VK_F3 : gdk.GDK_KEY_F3,
	VK_F4 : gdk.GDK_KEY_F4,
	VK_F5 : gdk.GDK_KEY_F5,
	VK_F6 : gdk.GDK_KEY_F6,
	VK_F7 : gdk.GDK_KEY_F7,
	VK_F8 : gdk.GDK_KEY_F8,
	VK_F9 : gdk.GDK_KEY_F9,
	VK_F10 : gdk.GDK_KEY_F10,
	VK_F11 : gdk.GDK_KEY_F11,
	VK_F12 : gdk.GDK_KEY_F12,
}
