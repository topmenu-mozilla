var EXPORTED_SYMBOLS = [ "gdk_pixbuf" ];

const LIBNAME = "gdk_pixbuf_xlib-2.0";
const ABIS = [ 0 ];

const Cu = Components.utils;
const Cc = Components.classes;
const Ci = Components.interfaces;

Cu.import("resource://gre/modules/ctypes.jsm");
Cu.import("chrome://topmenu/content/ctypes-utils.js");
Cu.import("chrome://topmenu/content/glib.js");
Cu.import("chrome://topmenu/content/gobject.js");

function defines(lib) {
	this.GdkPixbuf = gobject.GObject;
	this.GdkPixbufDestroyNotify = ctypes.FunctionType(ctypes.default_abi,
	                                                  ctypes.void_t,
                                                      [glib.guchar.ptr, glib.gpointer]);
	this.GdkColorspace = glib.guint;
	this.GDK_COLORSPACE_RGB = 0;

	lib.lazy_bind("gdk_pixbuf_new", this.GdkPixbuf.ptr, this.GdkColorspace, glib.gboolean, glib.gint, glib.gint, glib.gint);
	lib.lazy_bind("gdk_pixbuf_new_from_data", this.GdkPixbuf.ptr, glib.guchar.ptr, this.GdkColorspace, glib.gboolean, glib.gint, glib.gint, glib.gint, glib.gint, this.GdkPixbufDestroyNotify.ptr, glib.gpointer);
	lib.lazy_bind("gdk_pixbuf_copy", this.GdkPixbuf.ptr, this.GdkPixbuf.ptr);
}

new ctypes_library(LIBNAME, ABIS, defines, this);
