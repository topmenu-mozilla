"use strict";

var EXPORTED_SYMBOLS = [ "topmenu_client" ];

const Cu = Components.utils;
const Cc = Components.classes;
const Ci = Components.interfaces;

Cu.import("resource://gre/modules/ctypes.jsm");
Cu.import("chrome://topmenu/content/ctypes-utils.js");
Cu.import("chrome://topmenu/content/glib.js");
Cu.import("chrome://topmenu/content/gobject.js");
Cu.import("chrome://topmenu/content/gdk.js");
Cu.import("chrome://topmenu/content/gtk.js");

function defines(lib) {
	lib.lazy_bind("topmenu_client_connect_window_widget", ctypes.void_t, gdk.GdkWindow.ptr, gtk.GtkWidget.ptr);
	lib.lazy_bind("topmenu_client_disconnect_window", ctypes.void_t, gdk.GdkWindow.ptr);

	this.TopMenuAppMenuBar = gobject.GObject;
	lib.lazy_bind("topmenu_app_menu_bar_new", this.TopMenuAppMenuBar.ptr);
	lib.lazy_bind("topmenu_app_menu_bar_set_app_menu", ctypes.void_t, this.TopMenuAppMenuBar.ptr, gtk.GtkWidget.ptr);

	this.TopMenuMonitor = gobject.GObject;
	lib.lazy_bind("topmenu_monitor_get_instance", this.TopMenuMonitor.ptr);
	lib.lazy_bind("topmenu_monitor_is_topmenu_available", glib.gboolean, this.TopMenuMonitor.ptr);
}

new ctypes_library("topmenu-client-gtk2", [ 0 ], defines, this);
