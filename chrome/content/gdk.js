var EXPORTED_SYMBOLS = [ "gdk" ];

const LIBNAME = "gdk-x11-2.0";
const ABIS = [ 0 ];

const Cu = Components.utils;
const Cc = Components.classes;
const Ci = Components.interfaces;

Cu.import("resource://gre/modules/ctypes.jsm");
Cu.import("chrome://topmenu/content/ctypes-utils.js");
Cu.import("chrome://topmenu/content/glib.js");
Cu.import("chrome://topmenu/content/gobject.js");

function defines(lib) {
	this.GdkDrawable = gobject.GObject;
	this.GdkWindow = this.GdkDrawable;

	this.GdkModifierType = glib.guint;
	this.GDK_SHIFT_MASK = 1 << 0;
	this.GDK_LOCK_MASK = 1 << 1;
	this.GDK_CONTROL_MASK = 1 << 2;
	this.GDK_MOD1_MASK = 1 << 3;
	this.GDK_ALT_MASK = this.GDK_MOD1_MASK;
	this.GDK_MOD2_MASK = 1 << 4;
	this.GDK_MOD3_MASK = 1 << 5;
	this.GDK_SUPER_MASK = 1 << 26;
	this.GDK_META_MASK = 1 << 28;

	this.GDK_KEY_BackSpace = 0xff08;
	this.GDK_KEY_Tab = 0xff09;
	this.GDK_KEY_Return = 0xff0d;
	this.GDK_KEY_Escape = 0xff1b;
	this.GDK_KEY_Delete = 0xffff;
	this.GDK_KEY_Home = 0xff50;
	this.GDK_KEY_Left = 0xff51;
	this.GDK_KEY_Up = 0xff52;
	this.GDK_KEY_Right = 0xff53;
	this.GDK_KEY_Down = 0xff54;
	this.GDK_KEY_Prior = 0xff55;
	this.GDK_KEY_Page_Up = 0xff55;
	this.GDK_KEY_Next = 0xff56;
	this.GDK_KEY_Page_Down = 0xff56;
	this.GDK_KEY_End = 0xff57;
	this.GDK_KEY_F1 = 0xffbe;
	this.GDK_KEY_F2 = 0xffbf;
	this.GDK_KEY_F3 = 0xffc0;
	this.GDK_KEY_F4 = 0xffc1;
	this.GDK_KEY_F5 = 0xffc2;
	this.GDK_KEY_F6 = 0xffc3;
	this.GDK_KEY_F7 = 0xffc4;
	this.GDK_KEY_F8 = 0xffc5;
	this.GDK_KEY_F9 = 0xffc6;
	this.GDK_KEY_F10 = 0xffc7;
	this.GDK_KEY_F11 = 0xffc8;
	this.GDK_KEY_L1 = 0xffc8;
	this.GDK_KEY_F12 = 0xffc9;

	lib.lazy_bind("gdk_window_get_toplevel", this.GdkWindow.ptr, this.GdkWindow.ptr);

	lib.lazy_bind("gdk_unicode_to_keyval", glib.guint, glib.guint32);
}

new ctypes_library(LIBNAME, ABIS, defines, this);
