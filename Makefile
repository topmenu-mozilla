ZIP?=zip

# What goes inside the .xpi package
CONTENT:=install.rdf chrome.manifest bootstrap.js icon.png \
	$(wildcard chrome/content/*)

all: dist

# "dist" target builds .xpi extension package.
dist: topmenu.xpi
	
topmenu.xpi: $(CONTENT)
	$(ZIP) $(ZIPFLAGS) $@ $^

clean:
	rm -f topmenu.xpi

.PHONY: all dist clean
