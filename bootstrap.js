"use strict";

var Cc = Components.classes;
var Ci = Components.interfaces;
var Cu = Components.utils;

function loadIntoWindow(window) {
	if (!window)
		return;

	var log = Log4Moz.repository.getLogger("topmenu.Bootstrap");

	try {
		window.topmenuProxy = TopMenuService.createWindowProxy(window);
	} catch (ex) {
		log.warn("Could not create window proxy: " + ex);
	}
}

function unloadFromWindow(window) {
	if (!window)
		return;
	if (!window.topmenuProxy)
		return;

	var log = Log4Moz.repository.getLogger("topmenu.Bootstrap");

	try {
		window.topmenuProxy.unload();
		window.topmenuProxy.dispose();
	} catch (ex) {
		log.warn("Could not unload window proxy: " + ex);
	}

	window.topmenuProxy = null;
}

var windowListener = {
	onOpenWindow: function(window) {
		// Wait for the window to finish loading
		var domWindow = window.QueryInterface(Ci.nsIInterfaceRequestor).getInterface(Ci.nsIDOMWindowInternal || Ci.nsIDOMWindow);
		domWindow.addEventListener("load", function onLoad() {
			domWindow.removeEventListener("load", onLoad, false);
			domWindow.addEventListener("unload", function onUnload() {
				domWindow.removeEventListener("unload", onUnload, false);
				unloadFromWindow(domWindow);
			}, false);
			loadIntoWindow(domWindow);
		}, false);
	},

	onCloseWindow: function(window) {},
	onWindowTitleChange: function(window, title) {}
};

function startup(data, reason) {
	// Setup logging
	Cu.import("chrome://topmenu/content/log4moz.js");
	var formatter = new Log4Moz.BasicFormatter();
	var root = Log4Moz.repository.rootLogger;
	root.level = Log4Moz.Level.Warn;

	var capp = new Log4Moz.ConsoleAppender(formatter);
	capp.level = Log4Moz.Level.Warn;
    root.addAppender(capp);

	var log = Log4Moz.repository.getLogger("topmenu.Bootstrap");

	// Load module
	Cu.import("chrome://topmenu/content/topmenuservice.js");

	// Load into any existing windows
	var wm = Cc["@mozilla.org/appshell/window-mediator;1"].getService(Ci.nsIWindowMediator);
	var windows = wm.getEnumerator(null);
	while (windows.hasMoreElements()) {
		var domWindow = windows.getNext().QueryInterface(Ci.nsIDOMWindow);
		loadIntoWindow(domWindow);
	}

	// Load into any new windows
	wm.addListener(windowListener);
}

function shutdown(data, reason) {
	// When the application is shutting down we normally don't have to clean
	// up any UI changes made
	if (reason === APP_SHUTDOWN)
		return;

	var wm = Cc["@mozilla.org/appshell/window-mediator;1"].getService(Ci.nsIWindowMediator);

	// Stop listening for new windows
	wm.removeListener(windowListener);

	// Unload from any existing windows
	var windows = wm.getEnumerator(null);
	while (windows.hasMoreElements()) {
		var domWindow = windows.getNext().QueryInterface(Ci.nsIDOMWindow);
		unloadFromWindow(domWindow);
	}

	// Stop logging and unload modules
	Cu.unload("chrome://topmenu/content/topmenuservice.js");
	Cu.unload("chrome://topmenu/content/log4moz.js");
}

function install(data, reason) {

}

function uninstall(data, reason) {

}
